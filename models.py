# -*- coding: utf-8 -*-

from openerp import models, fields
#api

# class custom/sandbox/llu_mite(models.Model):
#     _name = 'custom/sandbox/llu_mite.custom/sandbox/llu_mite'

#     name = fields.Char()

class llu_mite_insect(models.Model):

    def on_change_probe(self, cr, uid, ids, probe_id, context=None):
        if not probe_id:
            return {}
        return {
            'value': {
                'probe_id': probe_id
            }
        }
    def on_change_accident(self, cr, uid, ids, accident_id, context=None):
        if not accident_id:
            return {}
        return {
            'value': {
                'accident_id': accident_id
            }
        }

    _name = 'llu_mite.insect'
    _description = 'Insects'
    _order = "name asc"
    name = fields.Char(string='Name')
    image = fields.Binary("Photo", help="Upload image of mite")
    description = fields.Text("Description")
    infected = fields.Selection([('infected', 'Yes'), ('not_infected','No')], 'Was the mite infected?')
    probe_id = fields.Many2one('llu_mite.probe', 'Probe')
    accident_id = fields.Many2one('llu_mite.accident', 'Accident')

class llu_mite_probe(models.Model):

    _name = 'llu_mite.probe'
    _description = 'Probes in specific locations'
    _order =   "name asc"

    name = fields.Char(string='Name')
    probe_type = fields.Selection([('not_infected', '2 x 2 m (for not infected forest)'), ('infected','10 x 10 m (for infected forest)')], 'Probe type(m2)')
    location_id = fields.Many2one('stock.location', string='Location')
    coordinates = fields.Char(string='Coordinates')
    probe_taken = fields.Date(string='Date the probe was taken')
    forester_id = fields.Many2one('hr.employee', string='Forester')
    mites = fields.One2many('llu_mite.insect', 'probe_id', 'Mites found in particular probe')

class llu_mite_accident(models.Model):

    _name = 'llu_mite.accident'
    _description = 'Accidents registered in specific locations'
    _order = "name asc"

    name = fields.Char(string='Name')
    location_id = fields.Many2one('stock.location', string='Location')
    coordinates = fields.Char(string='Coordinates')
    date = fields.Datetime(string='Date and time of accident')
    victim_id = fields.Many2one('hr.employee', string='Person who was bitten')
    doctor_id = fields.Many2one('hr.employee', string='Person who registered an accident')
    description = fields.Text("Description of an accident")
    mites = fields.One2many('llu_mite.insect', 'accident_id', 'Mites involved in particular accident')