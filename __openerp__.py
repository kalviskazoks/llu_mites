# -*- coding: utf-8 -*-
{
    'name': "LLU mites",

    'summary': """
        This is additional module for getting 10 points :)""",

    'description': """
        Module containts three classes - insects, probes and accidents. Each of them has particular properties.
    """,

    'author': "Kalvis Kazoks",
    'website': "http://www.llu.lv",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Insects',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr', 'stock'],

    # always loaded
    'data': [
        'security/llu_mite_security.xml',
        'security/ir.model.access.csv',
        #'templates.xml',
        #'llu_mite_data.xml',
        #'templates/llu_mite_views.xml',
        'templates/llu_mite_insect_views.xml',
        'templates/llu_mite_probe_views.xml',
        'templates/llu_mite_accident_views.xml',
        'templates/llu_mite_menu.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],
}